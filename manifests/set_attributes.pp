# @summary
#   Set attributes on the file and directories
#
# 
# Creates a record in a tmpfile.d $config_file, which tells systemd
# to set file attributes on a given file or directory.
#
# @example
#   tmpfiles::set_access { '/tmp/example_dir/*': }
#     config_file => 'example_config',
#     description => 'This is an example of setting file attributes.'
#     order       => 19,
#     attributes  => '+i-d',
#     recursive   => true
#   }
#
# @param path
#   Path to the directory to be cleaned up periodically.
#
# @param config_file
#   The name of a systemd configuration in /etc/tmpfiles.d. Do not specify a path.
#
# @param description
#   Description of the record, added as a comment to the configuation file.
#
# @param order
#   Order of the record in the configuration file.
#
# @param attributes
#   A list of attributes preceeded by a `+`, `-` or `=` sign depending on whether
#   an attribute has to set, removed or reset to a value.
#
# @param recursive
#   Set attributes on a directory recursively.
#
# @param on_boot
#   Act only once at boot time.
#
define tmpfiles::set_attributes (
  String[1] $config_file,
  Stdlib::Unixpath $path = $name,
  Optional[String] $description = undef,
  Integer[1] $order = 29,
  Tmpfiles::Attr $attributes,
  Boolean $recursive = false,
  Boolean $on_boot = false,
) {
  $on_boot_str = { true => '!', false => '' }[$on_boot]
  $type = { true => 'H', false => 'h' }[$recursive]

  systemd::tmpfile { "set_attributes_${name}":
    type        => "${type}${on_boot_str}",
    path        => $path,
    item_name   => $config_file,
    order       => String($order),
    argument    => $attributes,
    description => $description,
  }

  unless defined(Tmpfiles::Create[$config_file]) {
    tmpfiles::create { $config_file:
      require => Systemd::Tmpfile["set_attributes_${name}"]
    }
  }

}
