# @summary
#   Sets extended attributes on files and directories
#
# A description of what this defined type does
#
# @example
#   tmpfiles::set_extended_attributes { '/tmp/example_dir/*': }
#     config_file => 'example_config',
#     description => 'This is an example of setting extended attributtes.'
#     order       => 19,
#     attributes  => {
#       'user.foo' => 'bar',
#       'user.buz' => 'foo'
#     },
#     recursive   => true
#   }
#
# @param path
#   Path to the directory to be cleaned up periodically.
#
# @param config_file
#   The name of a systemd configuration in /etc/tmpfiles.d. Do not specify a path.
#
# @param description
#   Description of the record, added as a comment to the configuation file.
#
# @param order
#   Order of the record in the configuration file.
#
# @param attributes
#   A hash of the fully qualified extended attributes starting with the namespace
#   and their values.
#
# @param recursive
#   Set attributes on a directory recursively.
#
# @param on_boot
#   Act only once at boot time.
#
define tmpfiles::set_extended_attributes (
  String[1] $config_file,
  Hash[Tmpfiles::Xattr,String[1]] $attributes,
  Stdlib::Unixpath $path = $name,
  Optional[String] $description = undef,
  Integer[1] $order = 30,
  Boolean $recursive = false,
  Boolean $on_boot = false,
) {
  $on_boot_str = { true => '!', false => '' }[$on_boot]
  $type        = { true => 'T', false => 't' }[$recursive]

  $attributes_real = join($attributes.map |$item| { "${item[0]}=${item[1]}" }, ' ')

  systemd::tmpfile { "set_extended_attributes_${name}":
    type        => "${type}${on_boot_str}",
    path        => $path,
    item_name   => $config_file,
    order       => String($order),
    argument    => $attributes_real,
    description => $description,
  }

  unless defined(Tmpfiles::Create[$config_file]) {
    tmpfiles::create { $config_file:
      require => Systemd::Tmpfile["set_extended_attributes_${name}"]
    }
  }
}
