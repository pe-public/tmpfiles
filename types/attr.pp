# @summary
#   Regular expression representing a set of file attributes.
#
type Tmpfiles::Attr = Pattern[/^([\+\-\=][aAcCdDeijPsStTu]+)$/]
