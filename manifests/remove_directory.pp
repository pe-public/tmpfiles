# @summary
#   Creates a symbolic link to an existing file.
#
# Adds a configuration to systemd-tmpfiles, which creates a symbolic link
# to an existing file.
#
# @example
#   tmpfiles::remove_directory { '/tmp/example_dir': }
#     config_file => 'example_config',
#     description => 'Example of a directory deletion.'
#     order       => 12,
#     force       => true,
#   }
#
# @param path
#   Path to the directory to be cleaned up periodically.
#
# @param config_file
#   The name of a systemd configuration in /etc/tmpfiles.d.
#
# @param description
#   Description of the record, added as a comment to the configuation file.
#
# @param order
#   Order of the record in the configuration file.
#
# @param force
#   Overwrite existing file or directory with a symbolic link.
#
# @param on_boot
#   Act only once at boot time.
#
define tmpfiles::remove_directory (
  String[1] $config_file,
  Stdlib::Unixpath $path = $name,
  Optional[String] $description = undef,
  Boolean $force = true,
  Integer[1] $order = 26,
  Boolean $on_boot = false,
) {
  $on_boot_str = { true => '!', false => '' }[$on_boot]
  $type = { true => 'R', false => 'r' }[$force]

  systemd::tmpfile { "remove_directory_${name}":
    type        => "${type}${on_boot_str}",
    path        => $path,
    item_name   => $config_file,
    order       => String($order),
    description => $description,
  }

  unless defined(Tmpfiles::Create[$config_file]) {
    tmpfiles::create { $config_file:
      require => Systemd::Tmpfile["remove_directory_${name}"]
    }
  }
}
