# frozen_string_literal: true

require 'spec_helper'

describe 'tmpfiles::create_named_pipe' do
  RSpec::Puppet::Coverage.add_filter('Systemd::Tmpfile', 'create_named_pipe_/tmp/fifo')

  let(:title) { '/run/fifo' }
  let(:params) do
    {
      'description' => 'This is a test directory removal.',
      'order' => 30,
      'config_file' => 'tmp',
      'force' => false,
      'on_boot' => false,
    }
  end

  on_supported_os(get_distros).each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it { is_expected.to compile }
      it { is_expected.to contain_tmpfiles__create('tmp') }
      it { is_expected.to contain_exec('systemd-tmpfiles-tmp-create').with_command('systemd-tmpfiles --create /etc/tmpfiles.d/tmp.conf') }
      it { is_expected.to contain_systemd__tmpfile('create_named_pipe_/run/fifo').with_type('p') }

      context 'with config_file undefined' do
        let(:params) do
          super().merge({ 'config_file' => :undef })
        end

        it { is_expected.to compile.and_raise_error(%r{got Undef}) }
      end

      context 'with path in namevar' do
        let(:params) do
          super().merge({ 'path' => :undef })
        end

        it { is_expected.to contain_systemd__tmpfile('create_named_pipe_/run/fifo').with_path('/run/fifo') }
      end

      context 'with path in a parameter' do
        let(:title) { 'namevar' }
        let(:params) do
          super().merge({ 'path' => '/run/fifo' })
        end

        it { is_expected.to contain_systemd__tmpfile('create_named_pipe_namevar').with_path('/run/fifo') }
      end

      context 'with on_boot => true' do
        let(:params) do
          super().merge({ 'on_boot' => true })
        end

        it { is_expected.to contain_systemd__tmpfile('create_named_pipe_/run/fifo').with_type(%r{!$}) }
      end

      context 'with force => true' do
        let(:params) do
          super().merge({ 'force' => true })
        end

        it { is_expected.to contain_systemd__tmpfile('create_named_pipe_/run/fifo').with_type(%r{p\+}) }
      end
    end
  end
end
