# Changelog

All notable changes to this project will be documented in this file.

## Release 0.5.0

* Improve the unit tests.
* Fix a bug with positioning of the bang with on-boot configurations.

## Release 0.4.0

* Add systemd timer override for short time intervals.
* Properly validate for globs and string length.
* Move an exec to create objects to a separate resource.

## Release 0.3.0

* Add systemd timer override for short time intervals.

## Release 0.2.0

* Improve parameter validation.

## Release 0.1.0

* Initial release.
