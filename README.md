# tmpfiles

The module manages systemd-tmpfiles configuration.

## Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with tmpfiles](#setup)
    * [What tmpfiles affects](#what-tmpfiles-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with tmpfiles](#beginning-with-tmpfiles)
3. [Usage - Configuration options and additional functionality](#usage)
4. [Limitations - OS compatibility, etc.](#limitations)
5. [Development - Guide for contributing to the module](#development)

## Description

The module manages systemd-tmpfiles configuration. It allows to control
creation and timed cleanup of directories and files, create named pipes,
device files, adjust ownership and permissions, POSIX ACLs, regular and
extended attributes.

## Setup

### What tmpfiles affects 

The module creates configuration files in `/etc/tmpfiles.d` directory
possibly overriding the ones that shipped with the system.

If a directory clean interval requested in the parameters is less than a default
day, then a systemd timer override is created to trigger the
`systemd-tmpfiles-clean.timer` frequently enough to ensure that the shortest
requested interval is accomodated.

### Setup Requirements

The module depends on
[eyp-systemd](https://forge.puppet.com/modules/eyp/systemd) module.

### Beginning with tmpfiles

The simplest way to use the module is to include it in your manifest

```puppet
include tmpfiles
```

and configure using hiera. Alternatively individual defined types can be used
directly in the manifests:

```puppet
tmpfiles::create_directory { '/tmp':
  description => 'Clean up /tmp directory.',
  config_file => 'tmp',
  days        => 10,
  recursive   => true,
  removable   => true,
  create      => true,
  subvolume   => true,
  quota       => 'none'
}
```

## Usage

Confiruation is most convenient using hiera. The main `tmpfiles` class has a
single parameter `config`, which accepts the hash of configuration file names
(without extension). Each file name hash accepts a hash of configuration items,
like creating and cleaning up a directory, setting access rights, etc.

`tmpfiles` class provides two file-level configuration parameters that is not
offered by individual resources:

#### ensure

If `ensure` is set to *absent*, the given file is removed rather then created.
Default value for `ensure` is *present*.

#### override_partly

This parameter is used to partially override entries in vendor-provided
configuration files located in `/usr/lib/tmpfiles.d`. If set to `true`,
overriding file gets a suffix `-part`, otherwise it does not. Note that this
parameter is important even when you ensure that the configuration file should be
absent since it determines the name of the configuration file. The parameter
defaults to `false` making the new configuration override the entire
configuration file provided by vendor, if one exists.


```yaml
tmpfiles::config:      
  tmp:                  
    ensure: present          
    override_partly: false
    create_directory:      
      '/tmp':                                
        description: Clear tmp directories separately, to make them easier to
override
        order: 10   
        mode: '1777'                    
        owner: root      
        group: root
        days: 10
        subvolume: true            
        quota: inherit
        create: true    
      '/var/tmp':       
        order: 10
        mode: '1777' 
        owner: root                
        group: root
        days: 30
        subvolume: true      
        quota: inherit         
        create: true
    ignore_path:
      '/tmp/systemd-private-%b-*':
        description: Exclude namespace mountpoints created with PrivateTmp=yes
        recursive: false
      '/tmp/systemd-private-%b-*/tmp':
        recursive: true
      '/var/tmp/systemd-private-%b-*':
        recursive: false
      '/var/tmp/systemd-private-%b-*/tmp':
        recursive: true
    remove_directory:
      '/tmp/systemd-private-*':
        description: Remove top-level private temporary directories on each boot
        force: true
        on_boot: true
      '/var/tmp/systemd-private-*':
        force: true
        on_boot: true
  test_config:
    ensure: absent
    override_partly: false
```

The example above replicates the configuration shipped with Red Hat Linux in
`/usr/lib/tmpfiles.d/tmp.conf`. For full list of configurable resources see the
reference file.

## Limitations

1. Due to the limitation of the underlying `eyp-systemd` module a single
   configuration file cannot contain more than one configuration for a single file
   or directory. For instance, a configuration creating a directory and then
   setting POSIX ACLs on it.
2. Disabling individual configuration lines with `!` symbol is not implemented.
   It is just more straightforward just to comment them out in the hiera data
   source until you need them again.

## Development

Pull requests are welcome.

