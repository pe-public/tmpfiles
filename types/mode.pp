# @summary
#   Regular expression representing a file mode as a string with an additional
#   dash symbol being a default value.
#
type Tmpfiles::Mode = Pattern[/^([0-2]?[0-7]{3}|-)$/]
