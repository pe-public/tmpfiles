# @summary
#   Creates a new file with a set ownership, mode and content.
#
# A description of what this defined type does
#
# @example
#   tmpfiles::create_file { '/tmp/example_dir':
#     config_file => 'example_config',
#     description => 'This is an example of a file creation.'
#     order       => 11,
#     mode        => '0644',
#     owner       => 'johndoe',
#     group       => 'staff',
#     content     => 'This is a test file',
#     force       => true
#   }
#
# @param path
#   Path to the directory to be cleaned up periodically.
#
# @param config_file
#   The name of a systemd configuration in /etc/tmpfiles.d. Do not specify a path.
#
# @param description
#   Description of the record, added as a comment to the configuation file.
#
# @param order
#   Order of the record in the configuration file.
#
# @param content
#   The content of the file to be created or written to an existing file.
#
# @param mode
#   File mode of the directory to be created.
#
# @param owner
#   Owner of the directory to be created.
#
# @param group
#   Primary group for the directory to be created.
#
# @param force
#   Re-create file if it already exists.
#
# @param on_boot
#   Act only once at boot time.
#
define tmpfiles::create_file (
  String[1] $config_file,
  Stdlib::Unixpath $path = $name,
  Optional[String] $description = undef,
  Integer[1] $order = 22,
  Tmpfiles::Mode $mode = '-',
  String[1] $owner = '-',
  String[1] $group = '-',
  Optional[String] $content = undef,
  Boolean $force = false,
  Boolean $on_boot = false,
) {
  $on_boot_str = { true => '!', false => '' }[$on_boot]
  $type = { true => 'F', false => 'f' }[$force]

  systemd::tmpfile { "create_file_${name}":
    type        => "${type}${on_boot_str}",
    path        => $path,
    mode        => $mode,
    user        => $owner,
    group       => $group,
    argument    => $content,
    item_name   => $config_file,
    order       => String($order),
    description => $description,
  }

  unless defined(Tmpfiles::Create[$config_file]) {
    tmpfiles::create { $config_file:
      require => Systemd::Tmpfile["create_file_${name}"]
    }
  }

}
