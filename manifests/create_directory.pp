# @summary
#   Creates a record in systemd-tmpfiles configuration file describing
#   a temporary directory.
#
# Creates a record in a tmpfile.d $config_file, which tells systemd
# to optionally create and clean up the contents of a volatile directory.
#
# @example
#   tmpfiles::create_directory { '/tmp/example_dir': }
#     config_file => 'example_config',
#     description => 'This is an example of setting file attributes.'
#     mode        => '0755',
#     owner       => root,
#     group       => root,
#     days        => 10,
#     order       => 19,
#     recursive   => true
#   }
#
# @param path
#   Path to the directory to be cleaned up periodically. For existing directories
#   globs can be used.
#
# @param config_file
#   The name of a systemd configuration in /etc/tmpfiles.d.
#
# @param description
#   Description of the record, added as a comment to the configuation file.
#
# @param order
#   Order of the record in the configuration file.
#
# @param weeks
#   Once how many weeks the directory should be cleaned up
#
# @param days
#   Once how many days the directory should be cleaned up
#
# @param hours
#   Once how many hours the directory should be cleaned up
#
# @param minutes
#   Once how many minutes the directory should be cleaned up
#
# @param seconds
#   Once how many seconds the directory should be cleaned up
#
# @param mode
#   File mode of the directory to be created.
#
# @param owner
#   Owner of the directory to be created.
#
# @param group
#   Primary group for the directory to be created.
#
# @param recursive
#   Clean up all the subdirectories recursively as well.
#
# @param removable
#   The contents of the directory created to be removable using --remove option.
#
# @param create
#   Create directory, if it does not exist.
#
# @param subvolume
#   On btrfs file system create a subvolume instead. Valid only in the context of
#   subvolume creation, i.e. `create` set to *true*, cannot be used on existing ones.
#   To clean up the existing subvolumes, treat them as regular directory paths.
#
# @param quota
#   How quotas to be handled on btrfs: ignore them (none), inherit from the
#   container volume (inherit) or create a separate quota (separate). Applies only when
#   using btfrs volumes i.e. `subvolume` parameter is *true*.
#
# @param on_boot
#   Act only once at boot time.
#
define tmpfiles::create_directory (
  String[1] $config_file,
  Stdlib::Unixpath $path = $name,
  Optional[String] $description = undef,
  Integer[1] $order = 21,
  Tmpfiles::Mode $mode = '-',
  String[1] $owner = '-',
  String[1] $group = '-',
  Optional[Integer[1]] $weeks = undef,
  Optional[Integer[1]] $days = undef,
  Optional[Integer[1]] $hours = undef,
  Optional[Integer[1]] $minutes = undef,
  Optional[Integer[1]] $seconds = undef,
  Boolean $recursive = true,
  Boolean $removable = false,
  Boolean $create = true,
  Boolean $subvolume = false,
  Enum['none','inherit','separate'] $quota = 'none',
  Boolean $on_boot = false,
) {
  # construct age out of parts
  $age_filtered = {
    'w' => $weeks,
    'd' => $days,
    'h' => $hours,
    'm' => $minutes,
    's' => $seconds
  }.filter |$item| { $item[1] =~ NotUndef }

  if $age_filtered.empty {
    if $create {
      $age = '-'
    } else {
      fail('Age threshold must be specified.')
    }
  } else {
    $age = join($age_filtered.map |$item| { "${item[1]}${item[0]}" }, '')
  }

  # globs incompatible with directory creation
  $is_a_glob = $path =~ /[*?\[\]\{\}]/
  if $is_a_glob and $create {
    fail('Globs can be used only with existing directories.')
  }

  $on_boot_str   = { true => '!', false => '' }[$on_boot]
  $recursive_str = { true => '', false => '~' }[$recursive or ($age == '-')]

  if $create {
    if $subvolume {
      $type = { 'none' => 'v', 'inherit' => 'q', 'separate' => 'Q' }[$quota]
    } else {
      $type = { true => 'D', false => 'd' }[$removable]
    }
  } else {
    $type = 'e'
  }

  systemd::tmpfile { "create_directory_${name}":
    type        => "${type}${on_boot_str}",
    path        => $path,
    mode        => $mode,
    user        => $owner,
    group       => $group,
    age         => "${recursive_str}${age}",
    item_name   => $config_file,
    order       => String($order),
    description => $description,
  }

  unless defined(Tmpfiles::Create[$config_file]) {
    tmpfiles::create { $config_file:
      require => Systemd::Tmpfile["create_directory_${name}"]
    }
  }

}
