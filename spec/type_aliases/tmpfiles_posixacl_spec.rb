# frozen_string_literal: true

require 'spec_helper'

describe 'Tmpfiles::PosixAcl' do
  it { is_expected.to allow_value('d:group:org-admin_accts:r-x') }
  it { is_expected.to allow_value('d:g:admin:r-x') }
  it { is_expected.to allow_value('group:org-admin_accts:r-x') }
  it { is_expected.to allow_value('g:admin:r-x') }
  it { is_expected.to allow_value('d:user:admin:rwx') }
  it { is_expected.to allow_value('d:u:admin:rwx') }
  it { is_expected.to allow_value('user:admin:rwx') }
  it { is_expected.to allow_value('u:admin:rwx') }
  it { is_expected.not_to allow_value('anything') }
end
