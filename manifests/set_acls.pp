# @summary
#   Set POSIX ACLs on files and directories
#
# A description of what this defined type does
#
# @example
#   tmpfiles::set_acls { '/tmp/example_dir/*': }
#     config_file => 'example_config',
#     description => 'This is an example of setting extended attributtes.'
#     order       => 19,
#     acls        => [
#       'd:group:adm:r-x',
#       'd:group:wheel:r-x',
#       'group:adm:r-x',
#       'group:wheel:r-x'
#     ],
#     recursive   => true
#   }
#
# @param path
#   Path to the directory to be cleaned up periodically.
#
# @param config_file
#   The name of a systemd configuration in /etc/tmpfiles.d. Do not specify a path.
#
# @param description
#   Description of the record, added as a comment to the configuation file.
#
# @param order
#   Order of the record in the configuration file.
#
# @param acls
#   A list of POSIX ACLs to be applied to the directories or files.
#
# @param recursive
#   Set attributes on a directory recursively.
#
# @param replace
#   Replace existing ACLs with specified.
#
# @param on_boot
#   Act only once at boot time.
#
define tmpfiles::set_acls (
  String[1] $config_file,
  Array[Tmpfiles::PosixAcl] $acls,
  Stdlib::Unixpath $path = $name,
  Optional[String] $description = undef,
  Integer[1] $order = 28,
  Boolean $recursive = false,
  Boolean $replace = false,
  Boolean $on_boot = false,
) {
  $on_boot_str = { true => '!', false => '' }[$on_boot]
  $type        = { true => 'A', false => 'a' }[$recursive]
  $replace_str = { true => '', false => '+' }[$replace]

  $acls_real = join($acls, ',')

  systemd::tmpfile { "set_acls_${name}":
    type        => "${type}${replace_str}${on_boot_str}",
    path        => $path,
    item_name   => $config_file,
    order       => String($order),
    argument    => $acls_real,
    description => $description,
  }

  unless defined(Tmpfiles::Create[$config_file]) {
    tmpfiles::create { $config_file:
      require => Systemd::Tmpfile["set_acls_${name}"]
    }
  }
}
