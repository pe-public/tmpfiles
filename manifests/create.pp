# @summary
#   Force creation of all artifacts when a configuration file gets created
#   or updated.
#
# @api private
#
# @param config_file
#   The name of the configuration file in /etc/tmpfilles.d without extension.
#
define tmpfiles::create (
  String $config_file = $name
) {

  exec { "systemd-tmpfiles-${config_file}-create":
    path        => '/bin:/sbin:/usr/bin:/usr/sbin',
    command     => "systemd-tmpfiles --create /etc/tmpfiles.d/${config_file}.conf",
    subscribe   => Concat["/etc/tmpfiles.d/${config_file}.conf"],
    refreshonly => true,
  }
}
