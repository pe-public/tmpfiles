# @summary
#   Writes a string to an existing file.
#
# Adds a configuration to systemd-tmpfiles, which writes a string
# to an existing file.
#
# @example
#   tmpfiles::write_file { '/tmp/example_dir': }
#     config_file => 'example_config',
#     description => 'This is an example of a file creation.'
#     order       => 11,
#     content     => 'This is a test file',
#   }
#
# @param path
#   Path to the directory to be cleaned up periodically.
#
# @param config_file
#   The name of a systemd configuration in /etc/tmpfiles.d.
#
# @param description
#   Description of the record, added as a comment to the configuation file.
#
# @param order
#   Order of the record in the configuration file.
#
# @param append
#   If true, file would be appended rather than overwritten with the provided
#   content.
#
# @param content
#   The content of the file to be created or written to an existing file.
#
# @param on_boot
#   Act only once at boot time.
#
define tmpfiles::write_file (
  String[1] $config_file,
  Stdlib::Unixpath $path = $name,
  Optional[String] $description = undef,
  Integer[1] $order = 31,
  Boolean $append = false,
  Optional[String] $content = undef,
  Boolean $on_boot = false,
) {
  $on_boot_str = { true => '!', false => '' }[$on_boot]
  $type = { true => 'w+', false => 'w' }[$append]

  systemd::tmpfile { "write_file_${name}":
    type        => "${type}${on_boot_str}",
    path        => $path,
    argument    => $content,
    item_name   => $config_file,
    order       => String($order),
    description => $description,
  }

  unless defined(Tmpfiles::Create[$config_file]) {
    tmpfiles::create { $config_file:
      require => Systemd::Tmpfile["write_file_${name}"]
    }
  }
}
