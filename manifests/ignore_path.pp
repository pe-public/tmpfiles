# @summary
#   Ignores a specified path.
#
# Adds a configuration to systemd-tmpfiles, which creates a symbolic link
# to an existing file.
#
# @example
#   tmpfiles::ignore_path { '/tmp/example_path/*': }
#     config_file => 'example_config',
#     description => 'Example of a path to be ignored.'
#     order       => 11,
#   }
#
# @param path
#   Path to the directory to be cleaned up periodically.
#
# @param config_file
#   The name of a systemd configuration in /etc/tmpfiles.d.
#
# @param description
#   Description of the record, added as a comment to the configuation file.
#
# @param order
#   Order of the record in the configuration file.
#
# @param recursive
#   Ignore the content of the path recursively.
#
# @param on_boot
#   Act only once at boot time.
#
#
#
define tmpfiles::ignore_path (
  String[1] $config_file,
  Stdlib::Unixpath $path = $name,
  Optional[String] $description = undef,
  Boolean $recursive = true,
  Integer[1] $order = 25,
  Boolean $on_boot = false,
) {
  $on_boot_str = { true => '!', false => '' }[$on_boot]
  $type = { true => 'X', false => 'x' }[$recursive]

  systemd::tmpfile { "ignore_path_${name}":
    type        => "${type}${on_boot_str}",
    path        => $path,
    item_name   => $config_file,
    order       => String($order),
    description => $description,
  }

  unless defined(Tmpfiles::Create[$config_file]) {
    tmpfiles::create { $config_file:
      require => Systemd::Tmpfile["ignore_path_${name}"]
    }
  }
}
