# @summary
#   Adjusts modes of files and directories.
#
# A description of what this defined type does
#
# @example
#   tmpfiles::set_access { '/tmp/example_dir/*': }
#     config_file => 'example_config',
#     description => 'This is an example of mode adjustment.'
#     order       => 19,
#     mode        => '0644',
#     owner       => 'johndoe',
#     group       => 'staff',
#     recursive   => true
#   }
#
# @param path
#   Path to the directory to be cleaned up periodically.
#
# @param config_file
#   The name of a systemd configuration in /etc/tmpfiles.d. Do not specify a path.
#
# @param description
#   Description of the record, added as a comment to the configuation file.
#
# @param order
#   Order of the record in the configuration file.
#
# @param mode
#   File mode of the directory to be created.
#
# @param owner
#   Owner of the directory to be created.
#
# @param group
#   Primary group for the directory to be created.
#
# @param recursive
#   Apply access rights to a directory recursively.
#
# @param on_boot
#   Act only once at boot time.
#
define tmpfiles::set_access (
  String[1] $config_file,
  Stdlib::Unixpath $path = $name,
  Optional[String] $description = undef,
  Integer[1] $order = 27,
  Tmpfiles::Mode $mode = '-',
  String[1] $owner = '-',
  String[1] $group = '-',
  Boolean $recursive = false,
  Boolean $on_boot = false,
) {
  $on_boot_str = { true => '!', false => '' }[$on_boot]
  $type = { true => 'Z', false => 'z' }[$recursive]

  systemd::tmpfile { "set_access_${name}":
    type        => "${type}${on_boot_str}",
    path        => $path,
    mode        => $mode,
    user        => $owner,
    group       => $group,
    item_name   => $config_file,
    order       => String($order),
    description => $description,
  }

  unless defined(Tmpfiles::Create[$config_file]) {
    tmpfiles::create { $config_file:
      require => Systemd::Tmpfile["set_access_${name}"]
    }
  }
}
