# frozen_string_literal: true

require 'spec_helper'

# debian_and_redhat = { supported_os: [
# { 'operatingsystem' => 'Debian', 'operatingsystemrelease' => ['11'] },
# { 'operatingsystem' => 'RedHat', 'operatingsystemrelease' => ['8'] },
# ] }

describe 'tmpfiles' do
  let(:params) do
    {
      'config' => {
        'tmp' => {
          'ensure' => 'present',
          'override_partly' => true,
          'create_directory' => {
            '/tmp/test_dir6' => { 'days' => 10 }
          },
          'create_device' => {
            '/dev/gadget' => { 'major' => 4, 'minor' => 7, 'kind' => 'block' }
          },
          'create_file' => {
            '/tmp/test_file1.txt' => {}
          },
          'create_named_pipe' => {
            '/tmp/fifo' => {}
          },
          'create_symlink' => {
            '/tmp/symlink' => { 'target' => '/tmp/target.txt' }
          },
          'ignore_path' => {
            '/tmp/test_dir1' => {}
          },
          'remove_directory' => {
            '/tmp/test_dir2' => {}
          },
          'set_access' => {
            '/tmp/test_dir3' => {}
          },
          'set_acls' => {
            '/tmp/test_dir4' => { 'acls' => [ 'group:admins:r-x' ] }
          },
          'set_attributes' => {
            '/tmp/test_file2.txt' => { 'attributes' => '+i' }
          },
          'set_extended_attributes' => {
            '/tmp/test_file3.txt' => { 'attributes' => { 'user.custom' => 'foo' } }
          },
          'write_file' => {
            '/tmp/test_file4.txt' => { 'content' => 'test file' }
          }
        },
        'scratch' => {
          'ensure' => 'present',
          'create_directory' => { '/scratch' => { 'days' => 60 } }
        },
        'test1' => {
          'ensure' => 'absent',
          'override_partly' => true
        },
        'test2' => {
          'ensure' => 'absent',
          'override_partly' => false
        }
      }
    }
  end

  on_supported_os(get_distros).each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it { is_expected.to compile.with_all_deps }
      it {
        is_expected.to contain_tmpfiles__create_directory('/tmp/test_dir6')
        is_expected.to contain_tmpfiles__create_device('/dev/gadget')
        is_expected.to contain_tmpfiles__create_file('/tmp/test_file1.txt')
        is_expected.to contain_tmpfiles__create_named_pipe('/tmp/fifo')
        is_expected.to contain_tmpfiles__create_symlink('/tmp/symlink')
        is_expected.to contain_tmpfiles__ignore_path('/tmp/test_dir1')
        is_expected.to contain_tmpfiles__remove_directory('/tmp/test_dir2')
        is_expected.to contain_tmpfiles__set_access('/tmp/test_dir3')
        is_expected.to contain_tmpfiles__set_acls('/tmp/test_dir4')
        is_expected.to contain_tmpfiles__set_attributes('/tmp/test_file2.txt')
        is_expected.to contain_tmpfiles__set_extended_attributes('/tmp/test_file3.txt')
        is_expected.to contain_tmpfiles__write_file('/tmp/test_file4.txt')
      }
      it {
        is_expected.to contain_exec('systemd-tmpfiles-tmp-part-create').with_command('systemd-tmpfiles --create /etc/tmpfiles.d/tmp-part.conf')
        is_expected.to contain_exec('systemd-tmpfiles-scratch-create').with_command('systemd-tmpfiles --create /etc/tmpfiles.d/scratch.conf')
        is_expected.not_to contain_exec('systemd-tmpfiles-test1-create')
        is_expected.not_to contain_exec('systemd-tmpfiles-test2-create')
      }
      it {
        is_expected.to contain_file('/etc/tmpfiles.d/test1-part.conf').with_ensure('absent')
        is_expected.to contain_file('/etc/tmpfiles.d/test2.conf').with_ensure('absent')
      }
      it { is_expected.not_to contain_systemd__timer__dropin('systed-tmpfiles-clean') }

      context 'with interval shorter than default' do
        let(:params) do
          super().merge(
            'config' => {
              'scratch' => {
                'create_directory' => { '/scratch' => { 'hours' => 6 } }
              }
            },
          )
        end

        it { is_expected.to contain_systemd__timer__dropin('systemd-tmpfiles-clean').with_on_startup_sec(21_600) }
      end
    end
  end
end
