# @summary
#   Creates a named pipe
#
# Creates a record in systemd-tempfile configuration file to
# create a named pipe.
#
# @example
#   tmpfiles::create_named_pipe { '/run/fifo': }
#     config_file => 'example_config',
#     special     => 'pipe',
#     description => 'This is an example of a named pipe creation.'
#     order       => 11,
#     mode        => '0666',
#     owner       => 'www-data',
#     group       => 'www-data',
#     force       => true
#   }
#
# @param path
#   Path to the directory to be cleaned up periodically.
#
# @param config_file
#   The name of a systemd configuration in /etc/tmpfiles.d. Do not specify a path.
#
# @param description
#   Description of the record, added as a comment to the configuation file.
#
# @param order
#   Order of the record in the configuration file.
#
# @param mode
#   File mode of the file to be created.
#
# @param owner
#   Owner of the file to be created.
#
# @param group
#   Primary group for the file to be created.
#
# @param on_boot
#   Act only once at boot time.
#
# @param force
#   Re-create a device/pipe file if it already exists.
#
define tmpfiles::create_named_pipe (
  String[1] $config_file,
  Stdlib::Unixpath $path = $name,
  Optional[String] $description = undef,
  Integer $order = 20,
  Tmpfiles::Mode $mode = '-',
  String[1] $owner = '-',
  String[1] $group = '-',
  Boolean $force = false,
  Boolean $on_boot = false,
) {
  $on_boot_str = { true => '!', false => '' }[$on_boot]
  $type = { true => 'p+', false => 'p' }[$force]

  systemd::tmpfile { "create_named_pipe_${name}":
    type        => "${type}${on_boot_str}",
    path        => $path,
    mode        => $mode,
    user        => $owner,
    group       => $group,
    item_name   => $config_file,
    order       => String($order),
    description => $description,
  }

  unless defined(Tmpfiles::Create[$config_file]) {
    tmpfiles::create { $config_file:
      require => Systemd::Tmpfile["create_named_pipe_${name}"]
    }
  }
}

