# frozen_string_literal: true

require 'spec_helper'

describe 'Tmpfiles::Attr' do
  it { is_expected.to allow_value('+aAci') }
  it { is_expected.to allow_value('-aAci') }
  it { is_expected.to allow_value('=aAci') }
  it { is_expected.not_to allow_value('+a=A-c+i') }
  it { is_expected.not_to allow_value('aAcC') }
  it { is_expected.not_to allow_value('42') }
  it { is_expected.not_to allow_value('anything') }
  it { is_expected.not_to allow_value('-Z+z') }
  it { is_expected.not_to allow_value('-+a') }
end
