# frozen_string_literal: true

require 'spec_helper'

describe 'tmpfiles::create_file' do
  RSpec::Puppet::Coverage.add_filter('Systemd::Tmpfile', 'create_file_/tmp/test_file1.txt')

  let(:title) { '/tmp/testfile.txt' }
  let(:params) do
    {
      'description' => 'This is a test directory removal.',
      'order' => 30,
      'config_file' => 'tmp',
      'force' => false,
      'on_boot' => false,
    }
  end

  on_supported_os(get_distros).each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it { is_expected.to compile }
      it { is_expected.to contain_tmpfiles__create('tmp') }
      it { is_expected.to contain_exec('systemd-tmpfiles-tmp-create').with_command('systemd-tmpfiles --create /etc/tmpfiles.d/tmp.conf') }
      it { is_expected.to contain_systemd__tmpfile('create_file_/tmp/testfile.txt').with_path('/tmp/testfile.txt') }
      it { is_expected.to contain_systemd__tmpfile('create_file_/tmp/testfile.txt').with_type('f') }

      context 'with config_file undefined' do
        let(:params) do
          super().merge({ 'config_file' => :undef })
        end

        it { is_expected.to compile.and_raise_error(%r{got Undef}) }
      end

      context 'with path in a parameter' do
        let(:title) { 'namevar' }
        let(:params) do
          super().merge({ 'path' => '/tmp/testfile.txt' })
        end

        it { is_expected.to contain_systemd__tmpfile('create_file_namevar').with_path('/tmp/testfile.txt') }
      end

      context 'with on_boot => true' do
        let(:params) do
          super().merge({ 'on_boot' => true })
        end

        it { is_expected.to contain_systemd__tmpfile('create_file_/tmp/testfile.txt').with_type(%r{!$}) }
      end

      context 'with force => true' do
        let(:params) do
          super().merge({ 'force' => true })
        end

        it { is_expected.to contain_systemd__tmpfile('create_file_/tmp/testfile.txt').with_type(%r{F}) }
      end

      context 'with file content' do
        let(:params) do
          super().merge({ 'content' => 'Test file' })
        end

        it { is_expected.to contain_systemd__tmpfile('create_file_/tmp/testfile.txt').with_argument('Test file') }
      end
    end
  end
end
