# frozen_string_literal: true

require 'spec_helper'

describe 'tmpfiles::create_directory' do
  RSpec::Puppet::Coverage.add_filter('Systemd::Tmpfile', 'create_directory_/scratch')
  RSpec::Puppet::Coverage.add_filter('Systemd::Tmpfile', 'create_directory_/tmp/test_dir6')

  let(:title) { '/var/tmp' }
  let(:params) do
    {
      'description' => 'This is a test directory removal.',
      'order' => 30,
      'config_file' => 'tmp',
      'days' => 1,
      'hours' => 6,
      'on_boot' => false,
    }
  end

  on_supported_os(get_distros).each do |os, _os_facts|
    context "on #{os}" do
      it { is_expected.to compile }
      it { is_expected.to contain_tmpfiles__create('tmp') }
      it { is_expected.to contain_exec('systemd-tmpfiles-tmp-create').with_command('systemd-tmpfiles --create /etc/tmpfiles.d/tmp.conf') }
      it { is_expected.to contain_systemd__tmpfile('create_directory_/var/tmp').with_type(%r{[^!]$}) }
      it { is_expected.to contain_systemd__tmpfile('create_directory_/var/tmp').with_path('/var/tmp').with_age('1d6h') }

      context 'with config_file undefined' do
        let(:params) do
          super().merge({ 'config_file' => :undef })
        end

        it { is_expected.to compile.and_raise_error(%r{got Undef}) }
      end

      context 'with age undefined' do
        let(:params) do
          {
            weeks: :undef,
            days: :undef,
            hours: :undef,
            minutes: :undef,
            seconds: :undef,
            config_file: 'tmp',
            create: true
          }
        end

        it { is_expected.to contain_systemd__tmpfile('create_directory_/var/tmp').with_age('-') }
      end

      context 'with age undef and create => false' do
        let(:params) do
          {
            weeks: :undef,
            days: :undef,
            hours: :undef,
            minutes: :undef,
            seconds: :undef,
            config_file: 'tmp',
            create: false
          }
        end

        it { is_expected.to compile.and_raise_error(%r{Age threshold must be specified.}) }
      end

      context 'with path in a parameter' do
        let(:title) { 'namevar' }
        let(:params) do
          super().merge({ 'path' => '/var/tmp' })
        end

        it { is_expected.to contain_systemd__tmpfile('create_directory_namevar').with_path('/var/tmp') }
      end

      context 'with create => false' do
        let(:params) do
          super().merge({ 'create' => false })
        end

        it { is_expected.to contain_systemd__tmpfile('create_directory_/var/tmp').with_type(%r{e}) }
      end

      context 'with on_boot => true' do
        let(:params) do
          super().merge({ 'on_boot' => true })
        end

        it { is_expected.to contain_systemd__tmpfile('create_directory_/var/tmp').with_type(%r{!$}) }
      end

      context 'with recursive => false' do
        let(:params) do
          super().merge({ 'recursive' => false })
        end

        it { is_expected.to contain_systemd__tmpfile('create_directory_/var/tmp').with_age(%r{^~}) }
      end

      context 'with create => true' do
        let(:params) do
          super().merge({ 'create' => true })
        end

        context 'with subvolume => true, quota => none' do
          let(:params) do
            super().merge({ 'subvolume' => true, 'quota' => 'none' })
          end

          it { is_expected.to contain_systemd__tmpfile('create_directory_/var/tmp').with_type(%r{v}) }
        end

        context 'with subvolume => true, quota => inherit' do
          let(:params) do
            super().merge({ 'subvolume' => true, 'quota' => 'inherit' })
          end

          it { is_expected.to contain_systemd__tmpfile('create_directory_/var/tmp').with_type(%r{q}) }
        end

        context 'with subvolume => true, quota => separate' do
          let(:params) do
            super().merge({ 'subvolume' => true, 'quota' => 'separate' })
          end

          it { is_expected.to contain_systemd__tmpfile('create_directory_/var/tmp').with_type(%r{Q}) }
        end

        context 'with subvolume => false, removable => true' do
          let(:params) do
            super().merge({ 'subvolume' => false, 'removable' => true })
          end

          it { is_expected.to contain_systemd__tmpfile('create_directory_/var/tmp').with_type(%r{D}) }
        end

        context 'with subvolume => false, removable => false' do
          let(:params) do
            super().merge({ 'subvolume' => false, 'removable' => false })
          end

          it { is_expected.to contain_systemd__tmpfile('create_directory_/var/tmp').with_type(%r{d}) }
        end
      end
    end
  end
end
