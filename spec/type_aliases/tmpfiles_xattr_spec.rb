# frozen_string_literal: true

require 'spec_helper'

describe 'Tmpfiles::Xattr' do
  it { is_expected.to allow_value('user.custom') }
  it { is_expected.to allow_value('system.posix_acl_access') }
  it { is_expected.to allow_value('trusted.md5sum') }
  it { is_expected.to allow_value('security.selinux') }
  it { is_expected.not_to allow_value('anything') }
  it { is_expected.not_to allow_value('random.thing') }
  it { is_expected.not_to allow_value('user.custom.security.selinux') }
  it { is_expected.not_to allow_value(42) }
end
