# frozen_string_literal: true

def get_distros
  { supported_os: [
    { 'operatingsystem' => 'Debian', 'operatingsystemrelease' => ['11'] },
    { 'operatingsystem' => 'RedHat', 'operatingsystemrelease' => ['8'] },
  ] }
end

RSpec.configure do |c|
  c.after(:suite) do
    RSpec::Puppet::Coverage.report!
  end
end
