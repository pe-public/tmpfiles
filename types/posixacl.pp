# @summary
#   Regular expression representing a POSIX ACL.
#
type Tmpfiles::PosixAcl = Pattern[/^(d:)?(u|user|g|group):[\w\-]*:[rwx\-]{3}$/]
