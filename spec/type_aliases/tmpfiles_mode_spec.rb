# frozen_string_literal: true

require 'spec_helper'

describe 'Tmpfiles::Mode' do
  it { is_expected.to allow_value('-') }
  it { is_expected.to allow_value('0755') }
  it { is_expected.to allow_value('755') }
  it { is_expected.to allow_value('2755') }
  it { is_expected.not_to allow_value('-644') }
  it { is_expected.not_to allow_value('42') }
  it { is_expected.not_to allow_value('anything') }
  it { is_expected.not_to allow_value('27550') }
  it { is_expected.not_to allow_value('755-') }
  it { is_expected.not_to allow_value('75-5') }
  it { is_expected.not_to allow_value('7755') }
end
