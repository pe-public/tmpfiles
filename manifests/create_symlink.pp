# @summary
#   Creates a symbolic link to an existing file.
#
# Adds a configuration to systemd-tmpfiles, which creates a symbolic link
# to an existing file.
#
# @example
#   tmpfiles::write_file { '/tmp/example_file': }
#     config_file => 'example_config',
#     description => 'Example of a symlink createion.'
#     order       => 11,
#     target      => '/tmp/example_link',
#   }
#
# @param path
#   Path to the directory to be cleaned up periodically.
#
# @param config_file
#   The name of a systemd configuration in /etc/tmpfiles.d.
#
# @param description
#   Description of the record, added as a comment to the configuation file.
#
# @param order
#   Order of the record in the configuration file.
#
# @param target
#   The target of a symbolic link.
#
# @param force
#   Overwrite existing file or directory with a symbolic link.
#
# @param on_boot
#   Act only once at boot time.
#
define tmpfiles::create_symlink (
  String[1] $config_file,
  Stdlib::Unixpath $target,
  Stdlib::Unixpath $path = $name,
  Optional[String] $description = undef,
  Boolean $force = true,
  Integer[1] $order = 24,
  Boolean $on_boot = false,
) {
  $on_boot_str = { true => '!', false => '' }[$on_boot]
  $type = { true => 'L+', false => 'L' }[$force]

  systemd::tmpfile { "create_symlink_${name}":
    type        => "${type}${on_boot_str}",
    path        => $path,
    item_name   => $config_file,
    order       => String($order),
    argument    => $target,
    description => $description,
  }

  unless defined(Tmpfiles::Create[$config_file]) {
    tmpfiles::create { $config_file:
      require => Systemd::Tmpfile["create_symlink_${name}"]
    }
  }
}
