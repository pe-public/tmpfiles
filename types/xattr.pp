# @summary
#   Regular expression representing extended attributes.
#
type Tmpfiles::Xattr = Pattern[/^(user|trusted|security|system)\.[\w]+$/]
