# frozen_string_literal: true

require 'spec_helper'

describe 'tmpfiles::ignore_path' do
  RSpec::Puppet::Coverage.add_filter('Systemd::Tmpfile', 'ignore_path_/tmp/test_dir1')

  let(:title) { '/tmp/testdir' }
  let(:params) do
    {
      'description' => 'This is a test directory removal.',
      'order' => 30,
      'config_file' => 'tmp',
      'recursive' => false,
      'on_boot' => false,
    }
  end

  on_supported_os(get_distros).each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it { is_expected.to compile }
      it { is_expected.to contain_tmpfiles__create('tmp') }
      it { is_expected.to contain_exec('systemd-tmpfiles-tmp-create').with_command('systemd-tmpfiles --create /etc/tmpfiles.d/tmp.conf') }
      it { is_expected.to contain_systemd__tmpfile('ignore_path_/tmp/testdir').with_type(%r{x}) }
      it { is_expected.to contain_systemd__tmpfile('ignore_path_/tmp/testdir').with_type(%r{[^!]$}) }

      context 'with config_file undefined' do
        let(:params) do
          super().merge({ 'config_file' => :undef })
        end

        it { is_expected.to compile.and_raise_error(%r{got Undef}) }
      end

      context 'with path in namevar' do
        let(:params) do
          super().merge({ 'path' => :undef })
        end

        it { is_expected.to contain_systemd__tmpfile('ignore_path_/tmp/testdir').with_path('/tmp/testdir') }
      end

      context 'with path in a parameter' do
        let(:title) { 'namevar' }
        let(:params) do
          super().merge({ 'path' => '/tmp/testdir' })
        end

        it { is_expected.to contain_systemd__tmpfile('ignore_path_namevar').with_path('/tmp/testdir') }
      end

      context 'with on_boot => true' do
        let(:params) do
          super().merge({ 'on_boot' => true })
        end

        it { is_expected.to contain_systemd__tmpfile('ignore_path_/tmp/testdir').with_type(%r{!$}) }
      end

      context 'with recursive => true' do
        let(:params) do
          super().merge({ 'recursive' => true })
        end

        it { is_expected.to contain_systemd__tmpfile('ignore_path_/tmp/testdir').with_type(%r{X}) }
      end
    end
  end
end
