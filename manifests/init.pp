# @summary
#   Manages volatile files and directories using systemd
#
# This class collects the tmpfiles::config resources from hiera.
# The example of the hiera configuration:
#
# ```yaml
# tmpfiles::config:
#   tmp:
#     ensure: present
#     override_partly: true
#     create_directory:
#       '/tmp':
#         description: Override for temp direcories.
#         create: true
#         order: 10
#         mode: '1777'
#         owner: root
#         group: root
#         days: 10
#         subvolume: true
#         quota: inherit
#       '/var/tmp':
#         create: true
#         order: 11
#         mode: '1777'
#         owner: root
#         group: root
#         days 30
#         subvolume: true
#         quota: inherit
# ```
#
# @see https://www.freedesktop.org/software/systemd/man/systemd-tmpfiles.html
#
# @see https://www.freedesktop.org/software/systemd/man/tmpfiles.d.html
#
# @example
#   include tmpfiles
#
# @example
#   class { 'tmpfiles::config':
#     'tmp'               => {
#       'ensure'          => 'present',
#       'override_partly  => true,
#       'create_directory => {
#         '/tmp'          => {
#           'description' => 'Override for temp directories',
#           'create'      => true,
#           'order'       => 10,
#           'mode'        => '1777',
#           'owner'       => 'root',
#           'group'       => 'root',
#           'days'        => 10,
#           'subvolume'   => true,
#           'quota'       => 'inherit'
#         },
#         '/var/tmp'      => {
#           'create'      => true,
#           'order'       => 11,
#           'mode'        => '1777',
#           'owner'       => 'root',
#           'group'       => 'root',
#           'days'        => 30,
#           'subvolume'   => true,
#           'quota'       => 'inherit'
#         }
#       }
#     }
#   }
#
# @param [Hash] config
#   A hash of individual config file resources.
#
# @option config [Enum['absent','present']] ensure
#   Defines whether the configuration file should be present or
#   removed. Defaults to `present`.
#
# @option config [Boolean] override_partly
#   If set to `false`, the configuration file overrides the vendor-provided
#   configuration file completely, if one exists. If set to `true`, only
#   configurations defined in a file with this module override the vendor's. 
#   The configurations not covered are kept as vendor have them.
#
# @option config [Hash] create_directory
#   Hash of [`tmpfiles::create_directory`](#tmpfilescreate_directory) resources. Automatically age out the
#   content of directories and optionally create them.
#
# @option config [Hash] create_device
#   Hash of [`tmpfiles::create_device`](#tmpfilescreate_device) resources. Create block or character
#   device files.
#
# @option config [Hash] create_file
#   Hash of [`tmpfiles::create_file`](#tmpfilescreate_file) resources. Create or truncate files with
#   a given access permissions.
#
# @option config [Hash] create_named_pipe
#   Hash of [`tmpfiles::create_named_pipe`](#tmpfilescreate_named_pipe) resources. Create named pipe files.
#
# @option config [Hash] create_symlink
#   Hash of [`tmpfiles::create_symlink`](#tmpfilescreate_symlink) resources. Create symbolic links.
#
# @option config [Hash] ignore_path
#   Hash of [`tmpfiles::ignore_path`](#tmpfilesignore_path) resources. List directories which
#   should be ignored during the directory content aging.
#
# @option config [Hash] remove_directory
#   Hash of [`tmpfiles::remove_directory`](#tmpfilesremove_directory) resources. Removes empty directories
#   or optionally recursively removes them with their content.
#
# @option config [Hash] set_access
#   Hash of [`tmpfiles::set_access`](#tmpfilesset_access) resources. Set ownership and mode on
#   files and directories.
#
# @option config [Hash] set_acls
#   Hash of [`tmpfiles::set_acls`](#tmpfilesset_acls) resources. Set POSIX ACLs on files and
#   directories.
#
# @option config [Hash] set_attributes
#   Hash of [`tmpfiles::set_attributes`](#tmpfilesset_attributes) resources. Set attributes on files
#   and directories.
#
# @option config [Hash] set_extended_attributes
#   Hash of [`tmpfiles::set_extended_attributes`](#tmpfilesset_extended_attributes) resources. Set extended
#   attributes on files and directories.
#
# @option config [Hash] write_file
#   Hash of [`tmpfiles::write_file`](#tmpfileswrite_file) resources. Define a content of a file
#   to write or append.
#
#
class tmpfiles (
  $config = {}
){
  # If a directory cleanup interval is shorter than a day, we need to call systemd-tmpfiles
  # more frequently than the OS default. To do this we have to override the systemd-tmpfiles-cleanup.timer
  # unit with a minimal interval found in the data. The calculation of the interval follows the algorithm
  # "if there are seconds,then return the smallest number of seconds, if there are no seconds and only 
  # miunutes, return the smallest number of minutes, if there are no seconds or minutes, return the smallest
  # number of hours."
  #
  # Find out the smallest amount of time specified in the module's parameters for directory cleanup interval.
  #
  # We need to compare hours to minutes to seconds, so we better convert everything to seconds to compare
  # apples to apples.
  $multiplier = { seconds => 1, minutes => 60, hours => 3600 }

  # Coercion of the type into an Array is needed because of a bug in the Puppet DSL when reducing an Iterable.
  # https://tickets.puppetlabs.com/browse/PUP-11438
  $min_time = Array($config.tree_each(include_containers => false)).reduce(86400) |$memo, $value| {
    $key_name = $value[0][3]
    if ($config[$value[0][0]]['ensure'] != 'absent') and
      ('create_directory' == $value[0][1]) and
      ($key_name in ['hours', 'minutes', 'seconds'])
    {
      $key_value = $value[1] * $multiplier[$key_name]
      # Less precise variant: min($memo, $key_value)
      Integer(inline_template('<%= @key_value.gcd(@memo) %>'))
    } else {
      $memo
    }
  }

  # If minimal interval calculated is smaller than a full day (in seconds), then
  # create a systemd dropin to call the clean timer more frequently than a default
  # one day.
  if $min_time < 86400 {
    systemd::timer::dropin { 'systemd-tmpfiles-clean':
      on_startup_sec => String($min_time)
    }
  }

  # each config file
  $config.each |$cf| {
    # check if we need to override the configuration file partially or completely.
    # defaults to full override of the vendor's configuration file.
    if ('override_partly' in $cf[1]) and ($cf[1]['override_partly'] == true) {
      $config_file_name = "${cf[0]}-part"
    } else {
      $config_file_name = $cf[0]
    }

    # if a file hash contains 'ensure' set to 'absent', remove the config file
    # otherwise remove the 'ensure' key from the config file hash.
    if 'ensure' in $cf[1] {
      $ensure = $cf[1]['ensure']
    } else {
      $ensure = 'present'
    }

    # remove keys from the hash, which do not belong to resources.
    $config_file_hash = delete($cf[1], ['ensure', 'override_partly'])

    if $ensure != 'absent' {
      # each type of record
      $config_file_hash.each |$record_type| {
        $records_updated = $record_type[1].reduce ({}) |$m, $record| {
          $m + { $record[0] => $record[1] + {'config_file' => $config_file_name} }
        }
        # instantiate a resource for each record
        create_resources("tmpfiles::${record_type[0]}", $records_updated)
      }
    } else {
      file { "/etc/tmpfiles.d/${config_file_name}.conf": ensure => absent }
    }
  }
}
