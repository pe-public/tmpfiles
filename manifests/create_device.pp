# @summary
#   Creates a block or character device
#
# Creates a record in systemd-tempfile configuration file to
# create a block or character device.
#
# @example
#   tmpfiles::create_device { '/dev/disk33': }
#     config_file => 'example_config',
#     special     => 'block',
#     description => 'This is an example of a device creation.'
#     order       => 11,
#     mode        => '0666',
#     owner       => 'www-data',
#     group       => 'www-data',
#     major       => 4,
#     minor       => 7,
#     force       => true
#   }
#
# @param path
#   Path to the directory to be cleaned up periodically.
#
# @param config_file
#   The name of a systemd configuration in /etc/tmpfiles.d. Do not specify a path.
#
# @param description
#   Description of the record, added as a comment to the configuation file.
#
# @param kind
#   A type of a deivce, `character` or `block`.
#
# @param order
#   Order of the record in the configuration file.
#
# @param mode
#   File mode of the file to be created.
#
# @param owner
#   Owner of the file to be created.
#
# @param group
#   Primary group for the file to be created.
#
# @param major
#   Device's major (driver id).
#
# @param minor
#   Device's minor (device id).
#
# @param on_boot
#   Act only once at boot time.
#
# @param force
#   Re-create a device/pipe file if it already exists.
#
define tmpfiles::create_device (
  String[1] $config_file,
  Enum['block','character'] $kind,
  Integer[1,254] $major,
  Integer[0,255] $minor,
  Stdlib::Unixpath $path = $name,
  Optional[String] $description = undef,
  Integer[1] $order = 20,
  Tmpfiles::Mode $mode = '-',
  String[1] $owner = '-',
  String[1] $group = '-',
  Boolean $force = false,
  Boolean $on_boot = false,
) {
  $on_boot_str = { true => '!', false => '' }[$on_boot]
  $force_str = { true => '+', false => '' }[$force]
  $device = { 'block' => 'b', 'character' => 'c' }[$kind]
  $type = "${device}${force_str}"

  systemd::tmpfile { "create_device_${name}":
    type        => "${type}${on_boot_str}",
    path        => $path,
    mode        => $mode,
    user        => $owner,
    group       => $group,
    item_name   => $config_file,
    argument    => "${major}:${minor}",
    order       => String($order),
    description => $description,
  }

  unless defined(Tmpfiles::Create[$config_file]) {
    tmpfiles::create { $config_file:
      require => Systemd::Tmpfile["create_device_${name}"]
    }
  }
}

