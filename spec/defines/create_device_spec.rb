# frozen_string_literal: true

require 'spec_helper'

describe 'tmpfiles::create_device' do
  RSpec::Puppet::Coverage.add_filter('Systemd::Tmpfiles', 'create_device_/dev/gadget')

  let(:title) { '/dev/gadget' }
  let(:params) do
    {
      'description' => 'This is a device file creation.',
      'order' => 30,
      'config_file' => 'tmp',
      'kind' => 'block',
      'major' => 4,
      'minor' => 7,
      'force' => false,
      'on_boot' => false,
    }
  end

  on_supported_os(get_distros).each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it { is_expected.to compile }
      it { is_expected.to contain_tmpfiles__create('tmp') }
      it { is_expected.to contain_exec('systemd-tmpfiles-tmp-create').with_command('systemd-tmpfiles --create /etc/tmpfiles.d/tmp.conf') }
      it { is_expected.to contain_systemd__tmpfile('create_device_/dev/gadget').with_argument('4:7').with_type(%r{b}).with_path('/dev/gadget') }

      context 'with config_file undefined' do
        let(:params) do
          super().merge({ 'config_file' => :undef })
        end

        it { is_expected.to compile.and_raise_error(%r{got Undef}) }
      end

      context 'with major not given' do
        let(:params) do
          super().merge({ 'major' => :undef })
        end

        it { is_expected.to compile.and_raise_error(%r{got Undef}) }
      end

      context 'with minor not given' do
        let(:params) do
          super().merge({ 'minor' => :undef })
        end

        it { is_expected.to compile.and_raise_error(%r{got Undef}) }
      end

      context 'with kind of device not given' do
        let(:params) do
          super().merge({ 'kind' => :undef })
        end

        it { is_expected.to compile.and_raise_error(%r{got Undef}) }
      end

      context 'with kind => character' do
        let(:params) do
          super().merge({ 'kind' => 'character' })
        end

        it { is_expected.to contain_systemd__tmpfile('create_device_/dev/gadget').with_type(%r{c}) }
      end

      context 'with path in a parameter' do
        let(:title) { 'namevar' }
        let(:params) do
          super().merge({ 'path' => '/dev/gadget' })
        end

        it { is_expected.to contain_systemd__tmpfile('create_device_namevar').with_path('/dev/gadget') }
      end

      context 'with on_boot => true' do
        let(:params) do
          super().merge({ 'on_boot' => true })
        end

        it { is_expected.to contain_systemd__tmpfile('create_device_/dev/gadget').with_type(%r{!$}) }
      end
    end
  end
end
